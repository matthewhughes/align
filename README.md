# align

Align text around a string. See tests under `t/` for functional examples.

```
Usage: align [OPTIONS] [FILE...]

Options:
  -a --align-string STRING    The string to align text off. Default ":"
  -i --ignore-indentation     Ignore indentation when aligning
  -n --no-skip                Don't skip non-alignable lines
  -h --help                   Print this help
```
## Installation

Currently setup for GNU/Linux systems:

* `make`
* `make install`

And to install:

* `make uninstall`

## Usage

```
$ cat test.txt
first object: some value
a second object: another value
```

```
$ align -a ':' test.txt
first object   : some value
a second object: another value
```

### Skipping Blocks

By default `align` will not stop aligning a block when there is no align string
in that block, e.g.:

```
$ cat multiple_blocks.txt
first line : value
second line : another value
This line has no align string
third line here : maybe related to the above
```

```
$ align multiple_blocks.txt
first line : value
second line : another value
This line has no align string
third line here : maybe related to the above
```

You can override this with the `-n/--no-skip` flag:

```
$ align --no-skip multiple_blocks.txt
first line      : value
second line     : another value
This line has no align string
third line here : maybe related to the above
```

### Ignoring Indentation

Similarly, by default align will stop aligning when a change of indentation
occurs:

```
$ cat mixed_indentation.txt
Line without indent: some content
    Indented : more content
```

```
$ align mixed_indentation.txt
Line without indent: some content
    Indented : more content
```

Which can be changed by the `-i/--ignored-indentation` option:

```
$ align --ignore-indentation mixed_indentation.txt
Line without indent: some content
    Indented       : more content
```
