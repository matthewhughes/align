/*
 * This file is part of Align
 * Copyright © 2019-2020 Matthew Hughes
 *
 * Align is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Align is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#define _XOPEN_SOURCE
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

void
die(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
    exit(EXIT_FAILURE);
}

void *
align_malloc(size_t size)
{
    void *ptr = malloc(size);
    if (ptr == NULL)
        die("Error alloc-ing\n");
    return ptr;
}

void *
align_realloc(void *ptr, size_t size)
{
    void *new_ptr = realloc(ptr, size);
    if (new_ptr == NULL)
        die("Error realloc-ing\n");
    return new_ptr;
}

int
align_strnwidth(const char *s, size_t n)
{
    if (!s)
        return 0;

    wchar_t wc;
    int width;
    size_t num_byte;
    mbstate_t mbstate = { 0 };

    for (width = 0;
         n && (num_byte = mbrtowc(&wc, s, n, &mbstate));
         s += num_byte, n -= num_byte) {

        if (num_byte == (size_t) (-1) || num_byte == (size_t) (-2)) {
            if (num_byte == (size_t) (-1)) {
                memset(&mbstate, 0, sizeof(mbstate));
                num_byte = 1;
            } else              /* -2 */
                num_byte = n;   /* bail out? */
            width += 1;
            continue;
        }
        if (iswprint(wc))
            width += wcwidth(wc);
        else
            width += 1;

    }
    return width;
}
