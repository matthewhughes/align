/*
 * This file is part of Align
 * Copyright © 2019-2020 Matthew Hughes
 * 
 * Align is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Align is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE
#define MAXLINES 1024

#include <ctype.h>
#include <errno.h>
#include <locale.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "options.h"
#include "utils.h"

enum block_attributes
{
    SME_INDENT = 01,            /* lines have the same indentation */
};

typedef struct
{
    char **lines;               /* lines of text in block */
    int *inds;                  /* index (byte position) of align char in each line */
    int *offsets;               /* column of align-char for each line */
    int max_offset;
    size_t nlines;              /* #lines in block */
    char *algnstr;
    int ign_indent;
    int no_skip;
} block;

enum actions
{
    SKIP = 0,
    END_BLOCK,
    START_BLOCK,
    ADD_LINE,
};

unsigned int get_line_attrs(const char *, const char *);
FILE *get_input_file(const char *);
int get_action(const block *, const char *);
void append_line(block *, const char *);
void init_block(block *, const cli_options *);
void alloc_block(block *);
void realloc_block(block *, const int);
void close_block(block *);
void reset_block(block *);
void print_block(const block *);

int
main(int argc, char **argv)
{
    setlocale(LC_ALL, "");
    cli_options opts;

    process_args(argc, argv, &opts);
    block block;
    init_block(&block, &opts);

    for (int i = 0; opts.fnames[i] != NULL; i++) {
        alloc_block(&block);
        char *buf = NULL;
        size_t len = 0;
        ssize_t nread;
        FILE *fp;
        fp = get_input_file(opts.fnames[i]);

        while ((nread = getline(&buf, &len, fp)) != -1) {
            switch (get_action(&block, buf))
            {
            case ADD_LINE:
                append_line(&block, buf);
                break;
            case END_BLOCK:
                print_block(&block);
                reset_block(&block);
                printf("%s", buf);
                break;
            case START_BLOCK:
                print_block(&block);
                reset_block(&block);
                append_line(&block, buf);
                break;
            case SKIP:
                printf("%s", buf);
                break;
            }
        }
        if (block.nlines) {     /* maybe some trailing lines */
            print_block(&block);
            reset_block(&block);
        }
        free(buf);
        fclose(fp);
        close_block(&block);
    }

    free_opts(&opts);
    return EXIT_SUCCESS;
}

FILE *
get_input_file(const char *fname)
{
    FILE *fp;
    if (strcmp(fname, STDIN_STR)) {
        if ((fp = fopen(fname, "r")) == NULL)
            die("Error: Unable to open file %s: %s.\n",
                fname, strerror(errno));
    } else
        fp = stdin;
    return fp;
}

void
init_block(block *block, const cli_options *opts)
{
    block->algnstr = opts->algnstr;
    block->ign_indent = opts->ign_indent;
    block->no_skip = opts->no_skip;
    block->nlines = 0;
    block->max_offset = 0;
}

void
alloc_block(block *block)
{
    block->lines = (char **) align_malloc(MAXLINES * sizeof(char *));
    block->offsets = (int *) align_malloc(MAXLINES * sizeof(int));
    block->inds = (int *) align_malloc(MAXLINES * sizeof(int));
}

void
realloc_block(block *block, const int n)
{
    block->lines = align_realloc(block->lines, n * MAXLINES * sizeof(char *));
    block->offsets =
        (int *) align_realloc(block->offsets, n * MAXLINES * sizeof(int));
    block->inds =
        (int *) align_realloc(block->inds, n * MAXLINES * sizeof(int));
}

void
reset_block(block *block)
{
    for (size_t i = 0; i < block->nlines; i++)
        free(block->lines[i]);
    block->nlines = 0;
    block->max_offset = 0;
}

void
print_block(const block *block)
{
    for (unsigned int i = 0; i < block->nlines; i++) {
        const char *line = block->lines[i];
        const int columns = block->max_offset - block->offsets[i];
        for (int j = 0; line[j] != '\0'; j++) {
            if (j == block->inds[i])
                for (int k = 0; k < columns; k++)
                    putchar(' ');
            putchar(line[j]);
        }
    }
}

unsigned int
get_line_attrs(const char *ctl_line, const char *line)
{
    unsigned attrs = 0;

    if (!isspace(*ctl_line) && !isspace(*line))
        SET_BIT(attrs, SME_INDENT);
    else if (*ctl_line != *line)
        UNSET_BIT(attrs, SME_INDENT);   /* indent whitespace mismatch */
    else {
        while (*line++ == *ctl_line++) {
            ;
        }
        isspace(*line) || isspace(*ctl_line)
            ? (UNSET_BIT(attrs, SME_INDENT))
            : (SET_BIT(attrs, SME_INDENT));
    }
    return attrs;
}

int
get_action(const block *block, const char *line)
{
    int attrs;
    if (strstr(line, block->algnstr))
    {
        if (block->nlines) {
            attrs = get_line_attrs(block->lines[block->nlines - 1], line);
            if ((attrs ^ SME_INDENT) && !block->ign_indent)
                return START_BLOCK;
            else
                return ADD_LINE;
        } else
            return ADD_LINE;
    } else if (block->no_skip)
        return ADD_LINE;
    else
        return block->nlines ? END_BLOCK : SKIP;
}

void
append_line(block *block, const char *line)
{
    size_t nlines = block->nlines;

    if (nlines && nlines % MAXLINES == 0)
        realloc_block(block, nlines / MAXLINES + 1);
    if ((block->lines[nlines] = strdup(line)) == NULL)
        die("Error strdup()ing input\n");

    const char *loc = strstr(line, block->algnstr);
    block->inds[nlines] = loc - line;
    block->offsets[nlines] = align_strnwidth(line, block->inds[nlines]);
    block->max_offset = MAX(block->max_offset, block->offsets[nlines]);

    block->nlines++;
}

void
close_block(block *block)
{
    for (size_t i = 0; i < block->nlines; i++)
        free(block->lines[i]);
    free(block->lines);
    free(block->inds);
    free(block->offsets);
}
