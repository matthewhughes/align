/*
 * This file is part of align
 * Copyright © 2019-2020 Matthew Hughes
 * 
 * Align is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Align is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "options.h"
#include "utils.h"

static void set_defaults(cli_options *);
static void parse_args(int, char **, cli_options *);
static void read_filenames(cli_options *, char **, int);

static const char *align_usage = "\
Usage: align [OPTIONS] [FILE...]\n\
Align text around a string\n\
\n\
Options:\n\
  -a --align-string STRING    The string to align text off. Default \":\"\n\
  -i --ignore-indentation     Ignore indentation when aligning\n\
  -n --no-skip                Don't skip non-alignable lines\n\
  -h --help                   Print this help\n";

void
process_args(int argc, char **argv, cli_options *opts)
{
    set_defaults(opts);
    parse_args(argc, argv, opts);
}

static void
set_defaults(cli_options *opts)
{
    opts->algnstr = ":";
    opts->ign_indent = 0;
    opts->no_skip = 0;
    opts->fnames = NULL;
}

static void
parse_args(int argc, char **argv, cli_options *opts)
{
    int c;
    struct option long_opts[] = {
        {"align-string", required_argument, NULL, 'a'},
        {"file", required_argument, NULL, 'f'},
        {"ignore-indentation", no_argument, NULL, 'i'},
        {"no-skip", no_argument, NULL, 'n'},
        {"help", no_argument, NULL, 'h'},
        {0, 0, 0, 0},
    };
    int opt_index = 0;
    while ((c = getopt_long(argc, argv, "a:f:hin", long_opts, &opt_index))) {
        if (c == -1)
            break;

        switch (c) {
        case 'a':
            opts->algnstr = optarg;
            break;
        case 'i':
            opts->ign_indent = 1;
            break;
        case 'n':
            opts->no_skip = 1;
            break;
        case 'h':
            printf("%s", align_usage);
            exit(EXIT_SUCCESS);
        case '?':
            fprintf(stderr, "%s", align_usage);
            exit(EXIT_FAILURE);
        default:
            abort();
        }
    }

    int count;
    char **filenames;
    if (argc > optind) {
        count = argc - optind;
        filenames = argv + optind;
    } else {
        count = 1;
        char *str = (char *) STDIN_STR;
        filenames = &str;
    }
    read_filenames(opts, filenames, count);
}

static void
read_filenames(cli_options *opts, char **src, int nnames)
{
    if ((opts->fnames = calloc(sizeof(char *), nnames + 1)) == NULL)
        die("Error: failed to calloc block\n");

    int i;
    for (i = 0; i < nnames; i++)
        if ((opts->fnames[i] = strdup(src[i])) == NULL)
            die("Error: failed to strdup string\n");
    opts->fnames[i] = NULL;
}

void
free_opts(cli_options *opts)
{
    for (int i = 0; opts->fnames[i] != NULL; i++)
        free(opts->fnames[i]);
    free(opts->fnames);
}
