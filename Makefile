SRCDIR := src
INCDIR := include
TARGET := align
PREFIX ?= /usr/local
BASHCOMPDIR ?= /share/bash-completion/completions
MANDIR ?= /share/man/man1

INSTALL_DIR := $(DESTDIR)$(PREFIX)/bin
COMPLETION_DIR := $(DESTDIR)$(PREFIX)$(BASHCOMPDIR)
MAN_DIR := $(DESTDIR)$(PREFIX)$(MANDIR)
INSTALL_TRGT := $(INSTALL_DIR)/align
COMPLETION_TRGT := $(COMPLETION_DIR)/align
MAN_TRGT := $(MAN_DIR)/align.1

VPATH=$(SRCDIR)

INCLUDES := -I$(INCDIR)
WARNINGS := -pedantic -Wall -Wextra -Wformat-security
LANG_OPS := -std=c99
CFLAGS := $(CFLAGS) $(LANG_OPS) -Werror $(WARNINGS) $(INCLUDES)
DEBUGFLAGS := -g

SRCS := $(wildcard $(SRCDIR)/*.c)
HEADERS := $(wildcard $(INCDIR)/*.h)
OBJS := $(notdir $(SRCS:.c=.o))
C_FILES := $(SRCS) $(HEADERS)

.DEFAULT_GOAL := $(TARGET)

.PHONY: debug test test_memory install install-bin install-common install-man \
	uninstall clean format_code check_code_format
.INTERMEDIATE: $(OBJS)
.PRECIOUS: $(OBJS)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

debug: CFLAGS+=$(DEBUGFLAGS)
debug: clean $(TARGET)

format_code:
	indent $(C_FILES)

check_code_format:
	./dev-bin/check_code_format $(C_FILES)

test: $(TARGET)
	DATA_DIR="$(CURDIR)/t/data" scruf --indent "  " t/*.scf

test_memory: $(TARGET)
	./dev-bin/mem_check "$(CURDIR)/t/data" "$(CURDIR)/$(TARGET)"

install: install-bin install-completion install-man

install-bin: $(TARGET)
	install -v -d '$(INSTALL_DIR)' && \
		install -v '$(TARGET)' '$(INSTALL_TRGT)'

install-completion:
	install -v -d '$(COMPLETION_DIR)' && \
		install -m 0644 -v src/completion/align.bash-completion \
		'$(COMPLETION_TRGT)'

install-man:
	install -v -d '$(MAN_DIR)' && \
		install -m 0644 -v man/align.1 '$(MAN_TRGT)'

uninstall:
	rm -f \
		'$(INSTALL_TRGT)' \
		'$(COMPLETION_TRGT)' \
		'$(MAN_TRGT)'

clean:
	-rm -f $(OBJS) $(TARGET) $(DEPS)
