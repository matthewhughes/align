# Contributing

Pull requests welcome!

## Formatting Code

Install [GNU Indent](https://www.gnu.org/software/indent/) and run `make
format_code`. You can verify code is formatted correctly via `make
check_code_format`

## Testing

Install [`scruf`](https://pypi.org/project/scruf) and run `make test`.

Checks for memory leaks are available by installing
[`valgrind`](https://www.valgrind.org/) and running `make test_memory`
