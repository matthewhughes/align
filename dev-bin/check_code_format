#!/bin/bash

set -euo pipefail

files="$*"
INDENT_OUTPUT_SUFFIX='.indent'

usage () {
    echo "Usage: $0 [ files ]"
}

cleanup () {
    find ./ -type f -name "*$INDENT_OUTPUT_SUFFIX" -exec rm -f {} \;
}

is_formatted () {
    file_to_check="$1"
    indent_out_file="$file_to_check$INDENT_OUTPUT_SUFFIX"

    indent "$file_to_check" -o "$indent_out_file"

    diff "$file_to_check" "$indent_out_file" >/dev/null
}
trap "cleanup" EXIT ERR

if [ $# -eq 0 ]
then
    usage
    exit 1
fi

needs_format=0
set +e
for f in $files
do
    if ! is_formatted "$f"
    then
        echo "$f needs formatting" 1>&2
        needs_format=1
    fi
done
set -e

exit $needs_format
