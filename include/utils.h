/*
 * This file is part of align
 * Copyright © 2019-2020 Matthew Hughes
 *
 * Align is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Align is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILS_H
#define UTILS_H

#define SET_BIT(mask, bit) mask |= bit
#define UNSET_BIT(mask, bit) mask &= ~bit

#define STDIN_STR "-"
#define MAX(a,b) (a) > (b) ? (a) : (b)
void die(const char *, ...);
int align_strnwidth(const char *, size_t);
void *align_malloc(size_t);
void *align_realloc(void *, size_t);

#endif
