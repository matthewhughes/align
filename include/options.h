/*
 *
 * This file is part of align
 * Copyright © 2019-2020 Matthew Hughes
 * 
 * Align is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Align is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPTIONS_H
#define OPTIONS_H

typedef struct
{
    char *algnstr;              /* The string to align around */
    int ign_indent;             /* flag, whether blocks ignore indentation */
    int no_skip;                /* flag, whether to skip over non-alignable lines */
    char **fnames;
} cli_options;

void process_args(int, char **, cli_options *);
void free_opts(cli_options *);

#endif
